# **Designing a Web Page**

- Functional and non-functional requirements for the solution should be provided. 
- Web page design should be complete and unambiguous.
- HTML should be semantic, and classes should be used.
- CSS should be stored in external files and maps to HTML using classes.
- JS events should be used in a way to affect the functionality of a web page.

### **Functional Requirements**
The web page must:

- be a static web page of a SmallBusinessPlatform
- be a main page with random accounts
- be written using HTML, CSS, JS


### **Non-functional Requirements**
The web page should:

- be user-friendly
- be complete and unambiguous
- be able to open on varying computers